import React from 'react';
import { Route, Switch, BrowserRouter as Router } from 'react-router-dom';
import { connect } from 'react-redux';
import Header from 'components/Header';
import Loader from 'components/Loader';

import PrivateRouter from './PrivateRouter';

import StartPage from 'pages/StartPage';

import Main from './main';

const AppRouter = ({ authenticated }) => ([
  <Router>
    <Header />
    <Switch>
      <Route path='/login' component={StartPage} />
      <PrivateRouter path='/' authenticated={authenticated} component={Main} />
    </Switch>
  </Router>,
  <Loader />
]);

const mapStateToProps = ({ auth }) => ({
  authenticated: auth.authenticated,
});

export default connect(mapStateToProps, null)(AppRouter);
