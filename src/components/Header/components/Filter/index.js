import React, { Fragment } from 'react';
import * as S from './styled';

const Input = ({ label, onChangeField }) => (
  console.log("onChangeField", onChangeField),
  <Fragment>
    <S.Label>{label}</S.Label>
    <S.Field onChange={(e) => onChangeField(e.target.value)} type="date">
    </S.Field>
  </Fragment>
);

const Filter = ({ onChange, onChangeTo }) => (
  <S.FilterContainer>
    <S.InputContainer>
      <Input onChangeField={onChange} label="Date from" />
      <Input onChangeField={onChangeTo} label="Date to" />
    </S.InputContainer>
  </S.FilterContainer>
)

export default Filter;