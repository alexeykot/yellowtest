import React, { Fragment } from 'react';
import * as S from './styled';

import config from './config';

const InfoPage = () => (
  <Fragment>
    <S.Container>
      <S.TextWrapper>
        <S.Title>INFO</S.Title>
        <S.Text>
          {config.p1}
        </S.Text>
        <S.Text>
          {config.p2}
        </S.Text>
      </S.TextWrapper>
    </S.Container>
  </Fragment>

)

export default InfoPage;