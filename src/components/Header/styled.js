import styled from 'styled-components';
import { ReactComponent as LogoIcon } from 'assets/icons/logo.svg';

export const Container = styled.header`
  background-color: #7ed321;
  justify-content: space-between;
  padding: 37px 30px;
  display: flex;
  z-index: 1111;
  position: sticky;
  top: 0;
  @media (max-width: 960px) {
    padding: 20px 25px;
    background-color: ${({ isMenuVisible }) => isMenuVisible ? 'white' : '#7ed321'};
}
`;

export const Logo = styled(LogoIcon)`
  @media (max-width: 960px) {
    g {
      fill: ${({ isMenuVisible }) => isMenuVisible ? '#7ed321' : 'white'};
    }
}
`;

export const BurgerMenu = styled.img`
  display: none;
  width: 28px;
  height: 25px;
  @media (max-width: 960px) {
    display: flex;
    align-self: center;
    margin-left: 39pt;
  }
`;