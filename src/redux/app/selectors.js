import { createSelector } from 'reselect';

export const getLoadingState = createSelector(
  state => state.auth.loading,
  state => state.jogs.loading,
  (
    auth,
    jogs,
  ) =>
    (
      auth
      || jogs
    ),
);

export const selector = () => { };
