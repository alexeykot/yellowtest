import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import { ReactComponent as Bear } from 'assets/icons/bear-face.svg';
import { requestAuthenticate } from 'redux/auth';

import * as S from './styled';

class StartPage extends Component {
  componentDidUpdate() {
    const {
      authenticated,
      loading,
      history,
    } = this.props;

    if (authenticated && !loading) {
      history.push('/jogs');
    }
  }
  login = () => {
    this.props.requestAuthenticate();
  }
  
  render() {
    return (
      <Fragment>
        <S.Container>
          <S.ButtonWrapper>
            <Bear />
            <S.Button onClick={() => this.login()}>Let me in</S.Button>
          </S.ButtonWrapper>
        </S.Container>
      </Fragment>
    )
  }
}

const mapStateToProps = ({ auth }) => ({
  authenticated: auth.authenticated,
  error: auth.error.message || auth.error,
  loading: auth.loading,
});

const mapDispatchToProps = {
  requestAuthenticate,
};

export default connect(mapStateToProps, mapDispatchToProps)(StartPage);