import styled from 'styled-components';

export const Container = styled.div`
  min-height: 908px;
  display: flex;
  flex-direction: column;
  align-items: center;
  width: 100%;
  padding-top: 50px;
`;

export const TextWrapper = styled.div`
  margin-top: 50px;
  width: 50%;
  @media (max-width: 960px) {
    width: 70%;
  }
`;

export const Title = styled.div`
  font-size: 36px;
  font-weight: bold;
  margin-bottom: 15px;
  color: #7ed321;
`;

export const Text = styled.p`
  line-height: 1.71;
  letter-spacing: normal;
`