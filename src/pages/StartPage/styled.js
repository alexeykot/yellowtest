import styled from 'styled-components';


export const Container = styled.div`
  min-height: 100vh;
  display: flex;
  justify-content: center;
  align-items: center;
  width: 100%;
`;

export const ButtonWrapper = styled.div`
  width: 503px;
  height: 379px;
  border-radius: 44px;
  background-color: #e990f9;
  display: flex;
  flex-direction: column;
  align-items: center;
  & svg  {
    padding-top: 77px;
  }
  @media (max-width: 960px) {
    background-color: white;
    & svg * {
    fill: #e990f9;
  }
  }

`;


export const Button = styled.button`
  width: 151px;
  height: 60px;
  border-radius: 36px;
  margin-top: 56px;
  border: solid 3px #ffffff;
  background-color: transparent;
  color: white;
  font-size: 18px;
  font-weight: bold;
  @media (max-width: 960px) {
    border-color: #e990f9;
    color: #e990f9;
  }
`;
