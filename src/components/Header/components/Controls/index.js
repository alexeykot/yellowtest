import React from 'react';
import { buttonsConfig } from 'configs';

import menu from 'assets/images/menu.png';

import { ButtonContainer, Button, ButtonText, FilterButton, Container, BurgerMenu, CancelButton } from './styled';

const Controls = ({ onFilterPress, isFilterVisible, menuClose, menuOpen, isMenuVisible, location }) => (
  <Container>
    <ButtonContainer isMenuVisible={isMenuVisible}>
      {buttonsConfig.map(button => 
        <Button onClick={menuClose}>
          <ButtonText isActiveLink={location.pathname === `/${button.path}`} to={`${button.path}`}>{button.name}</ButtonText>
        </Button>
      )}
    </ButtonContainer>
    <FilterButton isMenuVisible={isMenuVisible} isFilterVisible={isFilterVisible} onClick={onFilterPress} />
    {isMenuVisible ? <CancelButton onClick={menuClose} /> : <BurgerMenu src={menu} onClick={menuOpen} />}
  </Container>
);

export default Controls;