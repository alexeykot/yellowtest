import React, { PureComponent } from 'react';
import { connect } from 'react-redux';

import { getLoadingState } from 'redux/app/selectors';
import { Container, Circle } from './styled';

const KEY_CODES = {
  f1: 112,
  f12: 123,
};

const blockKeyboardEvents = (event) => {
  if (!event.ctrlKey && !event.altKey && (event.keyCode < KEY_CODES.f1 || event.keyCode > KEY_CODES.f12)) {
    event.preventDefault();
    event.stopPropagation();
  }
};

class AppLoading extends PureComponent {

  componentWillReceiveProps(nextProps) {
    if (nextProps.loading !== this.props.loading) {
      if (nextProps.loading) {
        document.addEventListener('keypress', blockKeyboardEvents, true);
        document.addEventListener('keydown', blockKeyboardEvents, true);
      } else {
        document.removeEventListener('keypress', blockKeyboardEvents, true);
        document.removeEventListener('keydown', blockKeyboardEvents, true);
      }
    }
  }

  render() {
    return this.props.loading && <Container><Circle /></Container>;
  }
}

const mapStateToProps = state => ({
  loading: getLoadingState(state),
});

export default connect(mapStateToProps)(AppLoading);
