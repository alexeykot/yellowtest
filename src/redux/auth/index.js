import { call, put, select, takeEvery } from 'redux-saga/effects';

import http from 'services/http';
import { saveUser } from 'services/storage';

const AUTHENTICATE_REQUEST = 'auth/AUTHENTICATE_REQUEST';
const AUTHENTICATE_SUCCESS = 'auth/AUTHENTICATE_SUCCESS';
const AUTHENTICATE_FAILURE = 'auth/AUTHENTICATE_FAILURE';

const INITIAL_STATE = {
  authenticated: false,
  error: '',
  loading: false,
  user: {},
};

export default function reducer(state = INITIAL_STATE, action = {}) {
  switch (action.type) {
    case AUTHENTICATE_REQUEST:
      return { ...INITIAL_STATE, loading: true };
    case AUTHENTICATE_SUCCESS:
      return { ...state, authenticated: true, loading: false };
    case AUTHENTICATE_FAILURE:
      return { ...state, loading: false, error: action.payload.message };
    default:
      return state;
  }
}

// <<<ACTIONS>>>
export const requestAuthenticate = () => ({ type: AUTHENTICATE_REQUEST });

// <<<WORKERS>>>
function* authenticate() {
  try {
    const params = {
      uuid: 'hello',
    };

    const { data: { response: { access_token }} } = yield call(http.post, '/auth/uuidLogin', params);
    yield call(saveUser, access_token);
    yield put({ type: AUTHENTICATE_SUCCESS, payload: access_token });
  } catch (error) {
    console.log(error);
    yield put({ type: AUTHENTICATE_FAILURE, payload: error });
  }
}

// <<<WATCHERS>>>
export function* watchAuthenticate() {
  yield takeEvery(AUTHENTICATE_REQUEST, authenticate);
}
