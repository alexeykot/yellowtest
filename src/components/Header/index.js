import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';

import { requestFilterJogsFrom, requestFilterJogsTo } from 'redux/jogs';
import Controls from './components/Controls';
import Filter from './components/Filter';

import { Container, Logo } from './styled';

class Header extends Component {

  state = {
    isFilterVisible: false,
    isMenuVisible: false,
  };

  onFilterPress = () => this.setState({ isFilterVisible: !this.state.isFilterVisible});
  menuOpen = () => this.setState({ isMenuVisible: true });
  menuClose = () => this.setState({ isMenuVisible: false })

  render() {
    const { isFilterVisible, isMenuVisible } = this.state;
    const { authenticated, requestFilterJogsFrom, requestFilterJogsTo, location } = this.props;
    return (
      <Fragment>
        <Container isMenuVisible={isMenuVisible}>
        <Logo isMenuVisible={isMenuVisible} />
          {authenticated && 
          <Controls
            menuOpen={this.menuOpen}
            menuClose={this.menuClose}
            onFilterPress={this.onFilterPress}
            isFilterVisible={isFilterVisible}
            isMenuVisible={isMenuVisible}
            location={location}
          />}
        </Container>
        {isFilterVisible && <Filter onChange={requestFilterJogsFrom} onChangeTo={requestFilterJogsTo} />}
      </Fragment>
    );
  }
}

const mapStateToProps = ({ auth }) => ({
  authenticated: auth.authenticated,
});

const mapDispatchToProps = {
  requestFilterJogsFrom,
  requestFilterJogsTo,
};
const HeaderComponent = withRouter(Header);
export default connect(mapStateToProps, mapDispatchToProps)(HeaderComponent);
