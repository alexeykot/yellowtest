import React from 'react';
import * as S from './styled';

const TextInput = ({ label, onChange, name, value, type }) => (
  <S.Container>
    <S.Label>{label}</S.Label>
    <S.Input type={type} name={name} value={value} onChange={onChange} />
  </S.Container>
);

export default TextInput;