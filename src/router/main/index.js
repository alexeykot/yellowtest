import React, { PureComponent } from 'react';
import { Route, Switch } from 'react-router-dom';

import JogsPage from 'pages/JogsPage';
import InfoPage from 'pages/InfoPage';

class Main extends PureComponent {

  render() {

    return (
      <Switch>
        <Route path='/jogs' component={JogsPage} />
        <Route path='/info' component={InfoPage} />
      </Switch>
    );
  }
}

export default Main;
