import styled from 'styled-components';
import { ReactComponent as Add} from 'assets/icons/add.svg';

export const Container = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  width: 100%;
  padding-top: 50px;
`;

export const JogsList = styled.div`
  margin-top: 50px;
`;

export const AddButton = styled(Add)`
  display: ${({ formVisible }) => formVisible ? 'none' : 'block'};
  width: 60px;
  height: 60px;
  object-fit: contain;
  position: fixed;
  bottom: 40px;
  right: 40px;
  &:hover {
    opacity: 0.2;
    cursor: pointer;
  }
`;