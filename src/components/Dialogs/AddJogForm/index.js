import React, { Component } from 'react';
import { connect } from 'react-redux';
import cancel from 'assets/images/cancel.png';
import { requestAddJog, requestEditJog } from 'redux/jogs';
import moment from 'moment';

import { formatDate } from 'pages/JogsPage/helpers';
import TextInput from './components/TextInput';
import * as S from './styled';

class AddJogForm extends Component {

  state = {
    distance: "",
    time: "",
    date: "",
    jog_id: "",
    user_id: null,
  };

  componentWillMount() {
    if (this.props.selectedJog.id) {
      const { selectedJog: { distance, time, date, id, user_id }} = this.props;
      this.setState({ distance: distance, time: time, user_id: user_id, date: formatDate(date), jog_id: id })
    }
    
  }

  onSave = () => {
    const { distance, time, date } = this.state;
    const jog = {
      distance,
      time,
      date,
    };
    this.props.requestAddJog(jog);
    this.props.onClose();
  }

  onEdit = () => {
    const { distance, time, date, jog_id, user_id } = this.state;
    const jog = {
      distance,
      time,
      date,
      jog_id,
      user_id,
    };
    this.props.requestEditJog(jog);
    this.props.onClose();
  }

  onChange = e => this.setState({ [e.target.name]: e.target.value });

  render() {
    console.log(this.state);
    const { onClose, selectedJog  } = this.props;
    const { distance, time, date } = this.state;
    const isEnable= !!(distance && time && date);
    return (
      <S.Container>
        <S.CloseButton src={cancel} onClick={onClose} />
        <S.InputContainer>
          <TextInput type="number" name="distance" value={distance} label="Distance" onChange={this.onChange} />
          <TextInput type="number" name="time" value={time} label="Time" onChange={this.onChange} />
          <TextInput type="date" name="date" value={date} label="Date" onChange={this.onChange} />
          <S.SaveButton disabled={!isEnable} onClick={selectedJog.id ? this.onEdit : this.onSave}>Save</S.SaveButton>
        </S.InputContainer>
      </S.Container>
    )
  }
}

const mapDispatchToProps = {
  requestAddJog,
  requestEditJog,
}
export default connect(null, mapDispatchToProps)(AddJogForm);