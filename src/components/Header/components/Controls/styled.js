import styled from 'styled-components';
import { NavLink } from 'react-router-dom';
import { ReactComponent as Filter } from 'assets/icons/filter-active.svg';
import { ReactComponent as Cancel } from 'assets/icons/cancel.svg';

export const Container = styled.nav`
  display: flex;
  align-items: center;
`;

export const CancelButton = styled(Cancel)`
      display: none;
    @media (max-width: 960px) {
    display: block;
  }
`;
export const ButtonContainer = styled.ul`
  display: flex;
  @media (max-width: 960px) {
    display: flex;
    position: absolute;
    flex-direction: column;
    align-items: center;
    justify-content: center;
    top: ${({ isMenuVisible }) => isMenuVisible ? '64px' : '-110vh'};
    left: 0;
    min-height: 100vh;
    width: 100%;
    background-color: white;
    padding: 0;
  }
`;

export const Button = styled.li`
  list-style-type: none;
  position: relative;
  &:not(:first-child) {
    margin-left: 46px;
  }
  @media (max-width: 960px) {
    margin-left: 0 !important;
    &:not(:first-child) {
    margin-top: 30pt;
  }
  }
`;
//TODO: change to svg
export const FilterButton = styled(Filter)`
  margin-left: 56px;
  &:hover {
    cursor: pointer;
    opacity: 0.5;
  }
  path {
    fill: ${({ isFilterVisible }) => isFilterVisible ? '#62AA14' : 'white'} !important;
  }
  rect {
    stroke: ${({ isFilterVisible }) => isFilterVisible ? '#62AA14' : 'none'};
  }
  @media (max-width: 960px) {
    display: ${({ isMenuVisible }) => isMenuVisible ? 'none' : 'block'};
  }
`;

export const ButtonText = styled(NavLink)`
  font-size: 14px;
  font-weight: bold;
  color: white;
  text-transform: uppercase;
  text-decoration: none;
  :any-link {
    cursor: pointer;
  }
  ::before {
    content: "";
    position: absolute;
    display: block;
    bottom: -4px;
    left: 0;
    height: 3px;
    width: 100%;
    background: white;
    transform: scaleX(0);
    transform: ${({ isActiveLink }) => isActiveLink ? 'scaleX(1)' : 'scaleX(0)'};
    transition: transform 0.3s ease-out;
  }
  
  :hover {
    ::before {
      transform: scaleX(1);
    }
  }
  @media (max-width: 960px) {
    color: ${({ isActiveLink }) => isActiveLink ? '#7ed321' : 'black'};
    font-size: 25pt;
  }
`;

export const BurgerMenu = styled.img`
  display: none;
  width: 28px;
  height: 25px;
  margin-left: 39pt;
  @media (max-width: 960px) {
    display: block;
  }
`;