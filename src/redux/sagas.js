import { all, fork } from 'redux-saga/effects';

import * as authWatchers from './auth';
import * as jogsWatchers from './jogs';

export default function* root() {
  yield all([
    fork(authWatchers.watchAuthenticate),
    fork(jogsWatchers.watchGetJogs),
    fork(jogsWatchers.watchAddJog),
    fork(jogsWatchers.watchEditJog),
  ]);
}
