export const saveUser = (user) => {
  localStorage.setItem('Yellow-Test', JSON.stringify(user));
};

export const getUser = () => JSON.parse(localStorage.getItem('Yellow-Test'));

export const deleteUser = () => {
  localStorage.removeItem('Yellow-Test');
};
