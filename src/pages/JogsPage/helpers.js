import moment from 'moment';

export const calculateSpeed = (time, distance) => (distance/time);

export const formatDate = (date) => moment.unix(date).format("YYYY-MM-DD");