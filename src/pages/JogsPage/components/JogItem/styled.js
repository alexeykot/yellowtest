import styled from 'styled-components';
//TODO: fix grid
export const Container = styled.div`
  display: grid;
  grid-template-columns: min-content max-content;
  grid-column-gap: 46px;
  :hover {
    opacity: 0.5;
    cursor: pointer;
  }
  &:not(:last-child) {
    margin-bottom: 78px;
  }
`;

export const JogInfo = styled.div`
  display: grid;
  grid-row-gap: 12px;
`;

export const InfoLabel = styled.span`
  font-size: 14px;
  font-weight: 600;
`;

export const Date = styled.span`
  font-size: 14px;
  font-weight: normal;
  font-style: normal;
  font-stretch: normal;
  line-height: normal;
  letter-spacing: normal;
`;

export const InfoText = styled.span`
  font-size: 14px;
  font-weight: 500;
`;

export const JogImage = styled.img``;