import styled from 'styled-components';

export const Container = styled.div`
  margin-top: 50px;
  width: 307pt;
  height: 379px;
  border-radius: 44px;
  background-color: #7ed321;
  position: relative;
  display: flex;
  align-items: center;
  justify-content: center;
  flex-direction: column;
  @media (max-width: 375px) {
    width: 80%;
}
`;

export const InputContainer = styled.div`
  width: 236pt;
  display: grid;
  grid-row-gap: 25px;
  @media (max-width: 375px) {
    width: 80%;
}
`;

export const CloseButton = styled.img`
  position: absolute;
  top: 24px;
  right: 27px;
  width: 27px;
  height: 27px;
  object-fit: contain;
  &:hover {
    opacity: 0.2;
    cursor: pointer;
  }
`;

export const SaveButton = styled.button`
  width: 100%;
  height: 42px;
  border-radius: 25.2px;
  border: solid 2px #ffffff;
  background-color: transparent;
  color: white;
  font-size: 12.6px;
  font-weight: bold;
  opacity: ${({ disabled }) => disabled ? 0.5 : 1}
`;