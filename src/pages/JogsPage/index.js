import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import * as F from 'react-virtualized'

import { formatDate } from './helpers';
import { requestGetJogs } from 'redux/jogs';
import AddJogForm from 'components/Dialogs/AddJogForm';

import JogItem from './components/JogItem';

import * as S from './styled';

class JogsPage extends Component {

  state = {
    formVisible: false,
    jogs: [],
    selectedJog: {},
  };

  componentDidMount() {
    this.props.requestGetJogs();
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.jogs !== nextProps.jogs) {
      this.setState({ jogs: nextProps.jogs });
    }
    
    if (this.props.filterDateFrom !== nextProps.filterDateFrom) {
      let newJogs;
      if (!nextProps.filterDateFrom && this.props.filterDateTo) {
        newJogs = this.props.jogs.filter(jog => formatDate(jog.date) <= this.props.filterDateTo);
      } else if (this.props.filterDateTo) {
        newJogs = this.props.jogs.filter(jog => formatDate(jog.date) <= this.props.filterDateTo && formatDate(jog.date) >= nextProps.filterDateFrom) 
      } else {
        newJogs = this.props.jogs.filter(jog => formatDate(jog.date) >= nextProps.filterDateFrom);
      }
      return this.setState({ jogs: newJogs });
    }

    if (this.props.filterDateTo !== nextProps.filterDateTo) {
      let newJogs;
      if (!nextProps.filterDateTo && this.props.filterDateFrom) {
        newJogs = this.props.jogs.filter(jog => formatDate(jog.date) >= this.props.filterDateFrom);
      } else if (this.props.filterDateFrom) {
        newJogs = this.props.jogs.filter(jog => formatDate(jog.date) <= nextProps.filterDateTo && formatDate(jog.date) >= this.props.filterDateFrom) 
      } else {
        newJogs = this.props.jogs.filter(jog => formatDate(jog.date) <= nextProps.filterDateTo);
      }
      if (!nextProps.filterDateTo && !this.props.filterDateFrom) {
        return this.setState({ jogs: this.props.jogs })
      }
      return this.setState({ jogs: newJogs });
    }
  }

  onAddClick = () => this.setState({ formVisible: true , selectedJog: {} });
  onCloseClick = () => this.setState({ formVisible: false });
  onEditClick = (jog) => this.setState({ formVisible: true, selectedJog: jog });


  render() {
    const { formVisible, jogs, selectedJog } = this.state;
    return (
      <Fragment>
        <S.Container>
          {formVisible ? <AddJogForm onClose={this.onCloseClick} selectedJog={selectedJog ? selectedJog : null} /> :  
          <S.JogsList>
            {jogs.map(jog => <JogItem onEditClick={this.onEditClick} jog={jog}/>)}
          </S.JogsList>}
          <S.AddButton formVisible={formVisible} onClick={this.onAddClick} />
        </S.Container>
      </Fragment>
    )
  }
}

const mapStateToProps = ({ jogs }) => ({
  jogs: jogs.jogs,
  filterDateFrom: jogs.filterDateFrom,
  filterDateTo: jogs.filterDateTo,
});

const mapDispatchToProps = {
  requestGetJogs,
};

export default connect(mapStateToProps, mapDispatchToProps)(JogsPage);