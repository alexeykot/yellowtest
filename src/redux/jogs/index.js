import { call, put, takeEvery } from 'redux-saga/effects';

import http from 'services/http';

const GET_JOGS_REQUEST = 'jogs/GET_JOGS_REQUEST';
const GET_JOGS_SUCCESS = 'jogs/GET_JOGS_SUCCESS';
const GET_JOGS_FAILURE = 'jogs/GET_JOGS_FAILURE';

const ADD_JOG_REQUEST = 'jogs/ADD_JOG_REQUEST';
const ADD_JOG_SUCCESS = 'jogs/ADD_JOG_SUCCESS';
const ADD_JOG_FAILURE = 'jogs/ADD_JOG_FAILURE';

const EDIT_JOG_REQUEST = 'jogs/EDIT_JOG_REQUEST';
const EDIT_JOG_SUCCESS = 'jogs/EDIT_JOG_SUCCESS';
const EDIT_JOG_FAILURE = 'jogs/EDIT_JOG_FAILURE';

const FILTER_JOGS = 'jogs/FILTER_JOGS';
const FILTER_JOGS_TO = 'jogs/FILTER_JOGS_TO';


const INITIAL_STATE = {
  jogs: [],
  error: '',
  loading: false,
  filterDateFrom: "",
  filterDateTo: "",
};

export default function reducer(state = INITIAL_STATE, action = {}) {
  switch (action.type) {
    case GET_JOGS_REQUEST:
      return { ...state, loading: true };
    case GET_JOGS_SUCCESS:
      return { ...state, loading: false, jogs: action.payload };
    case GET_JOGS_FAILURE:
      return { ...state, loading: false, error: action.payload.message };
    case ADD_JOG_REQUEST:
      return { ...state, loading: true };
    case ADD_JOG_SUCCESS:
      return { ...state, loading: false };
    case ADD_JOG_FAILURE:
      return { ...state, loading: false, error: action.payload.message };
    case EDIT_JOG_REQUEST:
      return { ...state, loading: true };
    case EDIT_JOG_SUCCESS:
      return { ...state, loading: false };
    case EDIT_JOG_FAILURE:
      return { ...state, loading: false, error: action.payload.message };
    case FILTER_JOGS:
      return { ...state, filterDateFrom: action.payload };
    case FILTER_JOGS_TO:
      return { ...state, filterDateTo: action.payload };
    default:
      return state;
  }
}

// <<<ACTIONS>>>
export const requestGetJogs = () => ({ type: GET_JOGS_REQUEST });
export const requestAddJog = (jog) => ({ type: ADD_JOG_REQUEST, payload: jog });
export const requestEditJog = (jog) => ({ type: EDIT_JOG_REQUEST, payload: jog });
export const requestFilterJogsFrom = (date) => ({ type: FILTER_JOGS, payload: date })
export const requestFilterJogsTo = (date) => ({ type: FILTER_JOGS_TO, payload: date })

// <<<WORKERS>>>
function* getJogs() {
  try {
    const { data: { response: { jogs }} } = yield call(http.get, '/data/sync');
    yield put({ type: GET_JOGS_SUCCESS, payload: jogs });
  } catch (error) {
    console.log(error);
    yield put({ type: GET_JOGS_FAILURE, payload: error });
  }
}

function* addJog({ payload }) {
  try {
    yield call(http.post, '/data/jog', payload);
    yield put({ type: ADD_JOG_SUCCESS });
    yield call(getJogs);
  } catch (error) {
    console.log(error);
    yield put({ type: ADD_JOG_FAILURE, payload: error });
  }
}

function* editJog({ payload }) {
  try {
    console.log(payload)
    const data = yield call(http.put, '/data/jog', payload)
    yield put({ type: EDIT_JOG_SUCCESS });
    yield call(getJogs);
  } catch(error) {
    yield put({ type: EDIT_JOG_FAILURE, payload: error });
  }
}
// <<<WATCHERS>>>
export function* watchGetJogs() {
  yield takeEvery(GET_JOGS_REQUEST, getJogs);
}

export function* watchAddJog() {
  yield takeEvery(ADD_JOG_REQUEST, addJog)
}

export function* watchEditJog() {
  yield takeEvery(EDIT_JOG_REQUEST, editJog)
}