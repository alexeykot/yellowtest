import styled from 'styled-components';

export const Container = styled.div`
  display: flex;
  width: 100%;
  align-items: center;
  justify-content: space-between;
  @media (max-width: 960px) {
    flex-direction: column;
    align-items: flex-start;
  }
`;

export const Label = styled.span`
  font-size: 16px;
  font-weight: normal;
  font-style: normal;
  font-stretch: normal;
  line-height: normal;
  letter-spacing: normal;
`;

export const Input = styled.input`
  width: 70%;
  height: 31px;
  border-radius: 7px;
  border: solid 1px #979797;
  @media (max-width: 960px) {
    width: 100%;
    margin-top: 6pt;
  }
`;