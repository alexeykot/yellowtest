import React from 'react';
import moment from 'moment';
import * as S from './styled';
import { calculateSpeed } from  '../../helpers';
import icon from 'assets/images/icon.png';

const JogItem = ({ jog, onEditClick }) => (
  <S.Container onClick={() => onEditClick(jog)}>
    <S.JogImage src={icon} />
    <S.JogInfo>
      <S.Date>{moment.unix(jog.date).format("DD.MM.YYYY")}</S.Date>
      <S.InfoText><S.InfoLabel>Speed:</S.InfoLabel> {calculateSpeed(jog.time, jog.distance)}</S.InfoText>
      <S.InfoText><S.InfoLabel>Distance:</S.InfoLabel> {jog.distance}</S.InfoText>
      <S.InfoText><S.InfoLabel>Time:</S.InfoLabel> {jog.time}</S.InfoText>
    </S.JogInfo>
  </S.Container>
)

export default JogItem;