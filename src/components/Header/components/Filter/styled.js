import styled from 'styled-components';

export const FilterContainer = styled.div`
  width: 100%;
  height: 60px;
  background-color: #eaeaea;
  display: flex;
  justify-content: center;
  align-items: center;
  position: sticky;
  top: 130px;
  z-index: 11;
  @media (max-width: 960px) {
    top: 96px;
  }
`;

export const InputContainer = styled.div`
  @media (max-width: 375px) {
    display: flex;
    justify-content: space-evenly;
    align-items: center;
  }
  }
`;

export const Field = styled.input`
  width: 72pt;
  height: 31px;
  border-radius: 11px;
  border: solid 1px #979797;
  background-color: #ffffff;
  margin-left: 15px;
  &:not(:last-child) {
    margin-right: 45px;
  }
  @media (max-width: 375px) {
    width: 20%;
    &:not(:last-child) {
      margin-right: 0;
  }
  }
`;

export const Label = styled.label`
  font-size: 13px;
`;
